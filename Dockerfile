FROM microsoft/azure-cli

RUN apk add --update \
    dos2unix \
    && rm -rf /var/cache/apk/*

WORKDIR /app

ENTRYPOINT tail -f /dev/null