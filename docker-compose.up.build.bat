REM Run Dockerfile.build.bat first if you want to create your own Container Image.
cd %~dp0
docker rm -f setup_azure_vm_arm_cli-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
pause
docker exec -it setup_azure_vm_arm_cli-1 /bin/bash