# How to setup Azure VM with Azure Resource Manager template (Powershell) and Azure CLI

Usage:

- Run *docker-compose.up.bat* to start the workspace/sandbox container

How to start:
```
cd azure-template.001.win2019
dos2unix *
bash deploy.sh
```

This will start the deployment of a Windows 2019 Server. 
You will need to insert Azure subscription Id, and possible change region.


