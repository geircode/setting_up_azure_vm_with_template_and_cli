#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

az group delete --name myazurevm001-rg --no-wait
az group delete --name myazurevm002-rg --no-wait
